const path = require('path');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const productionConfig = require('./webpack.production');
const developmentConfig = require('./webpack.development');
const { pages } = require('./src/data/data');

const production = process.env.npm_lifecycle_event === 'build';
if (production) {
  process.env.NODE_ENV = 'production';
} else {
  process.env.NODE_ENV = 'development';
}

function createPages() {
  return pages.map(page => (
    new HtmlWebpackPlugin({
      filename: `${page.name}.html`,
      template: `./pages/${page.name}/${page.name}.pug`,
      inject: false,
      minify: {
        removeComments: production,
        minifyCSS: production,
        minifyJS: production,
        collapseWhitespace: production,
      },
    })));
}

const commonConfig = merge([
  {
    context: path.resolve(__dirname, 'src'),
    entry: {
      index: './pages/index/index',
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
    },
    optimization: {
      minimize: true,
      noEmitOnErrors: true,
      splitChunks: {
        chunks: 'all',
      },
      minimizer: [
        new UglifyJsPlugin({
          cache: true,
          parallel: true,
          uglifyOptions: {
            output: {
              comments: false,
            },
          },
        }),
      ],
    },
    module: {
      rules: [
        // JS
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: [
            'babel-loader',
          ],
        },
        // HTML
        {
          test: /\.(pug|jade)$/,
          exclude: /(node_modules|bower_components)/,
          use: [
            {
              loader: 'pug-loader',
            },
          ],
        },
        // CSS
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            MiniCssExtractPlugin.loader,
            {
              loader: 'css-loader',
              options: {
                importLoaders: 2,
              },
            },
            'postcss-loader',
            'sass-loader',
          ],
        },
      ],
    },
    plugins: [
      new CleanWebpackPlugin([path.resolve(__dirname, 'dist')]),
    ].concat(createPages()),
  },
]);

module.exports = (mode) => {
  if (mode === 'production') {
    return merge(commonConfig, productionConfig, { mode });
  }
  return merge(commonConfig, developmentConfig, { mode });
};
