// import 'uikit';
// import '../../styles/custom.scss';
import fonImage from '../../images/fon/index-fon@1x.jpg';
import retinaFonImage from '../../images/fon/index-fon@2x.jpg';

function retinaCheck() {
  return matchMedia('(-webkit-min-device-pixel-ratio: 2), (min-device-pixel-ratio: 2), (min-resolution: 192dpi)').matches;
}

const mobile = window.matchMedia('(max-width: 959px)');

function imageLoad() {
  if (mobile.matches) {
    const img = new Image();
    if (retinaCheck()) {
      img.src = retinaFonImage;
    } else {
      img.src = fonImage;
    }
    img.onload = () => {
      document.body.style.backgroundImage = `url(${img.src})`;
    };
  }
}

mobile.addListener(imageLoad);

imageLoad();
