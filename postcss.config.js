module.exports = {
  plugins: {
    'postcss-import': { stage: 0 },
    'postcss-preset-env': {},
    cssnano: {
      preset: ['default', {
        discardComments: {
          removeAll: true,
        },
      }],
    },
  },
};
