const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const SitemapPlugin = require('sitemap-webpack-plugin').default;
const RobotstxtPlugin = require('robotstxt-webpack-plugin').default;
const AddAssetPlugin = require('add-asset-webpack-plugin');
const { common } = require('./src/data/data');

module.exports = {
  mode: 'production',
  output: {
    filename: './scripts/[name].[contenthash:4].js',
    chunkFilename: './scripts/[name].[contenthash:4].js',
  },
  module: {
    rules: [
      // Images
      {
        test: /\.(png|jpg|gif|svg|ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[hash:4].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles/[name].[contenthash:4].css',
      chunkFilename: 'styles/[name].[contenthash:4].css',
    }),
    new SitemapPlugin(common.url, ['/'], {
      lastMod: true,
      changeFreq: 'always',
      priority: '1',
    }),
    new RobotstxtPlugin({
      policy: [
        {
          userAgent: '*',
          disallow: '/',
          crawlDelay: 1,
        },
      ],
      sitemap: `${common.url}/sitemap.xml.gz`,
      host: common.url,
    }),
    new AddAssetPlugin('humans.txt', `/* TEAM */\nDeveloper: ${common.author}\nSite: ${common.authorEmail}\nLocation: Saint Petersburg, Russia\n\n/* SITE */\nLast update: ${new Date().toLocaleDateString(common.lang, { year: 'numeric', month: '2-digit', day: '2-digit' })}\nLanguage: Russian\nStandards: HTML5, CSS3, ES6\nIDE: WebStorm`),
  ],
};
