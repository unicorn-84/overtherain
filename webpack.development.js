const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  mode: 'development',
  output: {
    filename: './scripts/[name].js',
    chunkFilename: './scripts/[name].js',
  },
  devtool: 'none',
  devServer: {
    stats: 'errors-only',
    overlay: true,
    host: process.env.HOST,
    port: '9000',
  },
  module: {
    rules: [
      // Images
      {
        test: /\.(png|jpg|gif|svg|ico)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]',
            },
          },
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: 'styles/[name].css',
      chunkFilename: 'styles/[name].css',
    }),
  ],
};
